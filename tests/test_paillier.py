import random
from collections import Counter
import pytest

from swartz import paillier

params = (20, 30)


@pytest.fixture(name="election")
def fixture_election():
    # known to work:
    # modulus = (47, 23, 59, 29)
    # modulus = (5, 2, 7, 3)
    modulus = paillier.generate_rsa_modulus(128)
    return paillier.generate_key(modulus, params)


def test_vote(election):
    key, shares = election.key, election.shares
    message = random.randrange(1, key.n)

    cipher = key.encrypt(message)
    assert message != cipher

    selected = random.sample(shares, params[0])
    ci = [paillier.share_decryption(cipher, share, key, params) for share in selected]

    decryption = election.combine_shares([s for s, _ in ci])
    assert decryption == message


def test_election(election):
    key, shares = election.key, election.shares

    options = ["Jim", "Dwight", "Andy"]
    messages = {option: [int(i == j) for j in range(len(options))] for i, option in enumerate(options)}
    plain_votes = random.choices(options, k=1000)

    votes = [key.encrypt_ballot(messages[choice]) for choice in plain_votes]
    result = key.partial_sum(votes)

    selected = random.sample(shares, params[0])
    decryption_shares = [[paillier.share_decryption(cipher, share, key, params) for share in selected] for cipher in result]
    decryption = {option: election.combine_shares([s for s, _ in ci]) for option, ci in zip(options, decryption_shares)}

    expected = dict(Counter(plain_votes))
    assert expected == decryption


# def test_election_score(election):
#     key, shares = election.key, election.shares
#     min_score = 0
#     max_score = 5

#     options = ['Jim', 'Dwight', 'Andy']
#     raw_votes = [[5, 3, 0], [4, 5, 1], [1, 0, 5]] * 1000
#     factor = key.n // (max_score - min_score)
#     plain_votes = [[v * factor for v in vote] for vote in raw_votes]
#     votes = [key.encrypt_ballot(vote) for vote in plain_votes]
#     result = key.partial_sum(votes)

#     selected = random.sample(shares, params[0])
#     decryption_shares = [
#         [
#             paillier.share_decryption(cipher, share, key, params)
#             for share in selected
#         ] for cipher in result
#     ]
#     decryption = {
#         option: election.combine_shares([s for s, _ in ci]) / factor
#         for option, ci in zip(options, decryption_shares)
#     }

#     expected = dict(Counter(plain_votes))
#     assert expected == decryption
