from math import factorial
from typing import Collection, Generator

from .election import ThresholdElection
from .shamir import generate_shares
from .types import Decryption, Params, RSAModulus, Share, ThresholdPublicKey, VerificationKey
from .primes import generate_safe_prime

DEFAULT_KEYSIZE = 2048


def generate_rsa_modulus(length: int) -> RSAModulus:
    """Generates a RSA modulus (p', q' safe primes and
    p = 2p' + 1, q = 2q' + 1 where p and q are also prime)

    >>> p, p1, q, q1 = generate_rsa_modulus(128)
    >>> p == 2 * p1 + 1
    True
    >>> q == 2 * q1 + 1
    True
    >>> (p - 1) * (q - 1) == 4 * p1 * q1
    True
    """
    p, p1 = generate_safe_prime(length // 2)
    q, q1 = generate_safe_prime(length // 2)
    while p1 == q1:  # pragma: no cover
        q, q1 = generate_safe_prime(length // 2)
    return p, p1, q, q1


def secret(modulus: RSAModulus) -> int:
    """Choose the secret d for given RSA modulus

    >>> modulus = (5, 2, 7, 3)
    >>> secret(modulus)
    36
    """
    p, p1, q, q1 = modulus
    m = p1 * q1
    return m * pow(m, -1, p * q)


def verification_keys(shares: Collection[Share], v: int, ns1: int) -> Generator[VerificationKey, None, None]:
    Δ = factorial(len(shares))
    for i, s in shares:
        yield (i, pow(v, Δ * s, ns1))


def generate_key(modulus: RSAModulus, params: Params) -> ThresholdElection:
    p, p1, q, q1 = modulus
    n = p * q

    # cramer, damgard & nielsen
    d = secret(modulus)

    shares = [*generate_shares(d, params, n * p1 * q1)]

    v = pow(p, -1, q) ** 2
    vks = [*verification_keys(shares, v, n * n)]

    publ = ThresholdPublicKey(p, q, v)
    return ThresholdElection(params, publ, vks, shares)


def share_decryption(cipher: int, share: Share, key: ThresholdPublicKey, params: Params) -> Decryption:
    """
    Values from https://crypto.stackexchange.com/q/59636
    >>> cipher = 234
    >>> key = ThresholdPublicKey(5, 7, 0)
    >>> params = (3, 3)
    >>> share_decryption(cipher, (1, 159), key, params)
    Decryption(share=(1, 1121), proof=0)
    >>> share_decryption(cipher, (2, 94), key, params)
    Decryption(share=(2, 771), proof=0)
    >>> share_decryption(cipher, (3, 51), key, params)
    Decryption(share=(3, 106), proof=0)
    """
    i, s = share
    _, servers = params

    ns1 = key.ns1
    c = pow(cipher, 2 * factorial(servers) * s, ns1)
    proof = pow(key.v, s, ns1)
    return Decryption((i, c), proof)
