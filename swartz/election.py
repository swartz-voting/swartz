from typing import Collection, NamedTuple

from .shamir import combine_threshold_shares
from .types import Params, Share, ThresholdPublicKey, VerificationKey


class ThresholdElection(NamedTuple):
    params: Params
    key: ThresholdPublicKey
    verification_keys: Collection[VerificationKey]
    shares: Collection[Share]

    def combine_shares(self, shares: Collection[Share]):
        return combine_threshold_shares(shares, self.key, self.params)
