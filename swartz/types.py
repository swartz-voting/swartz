from dataclasses import dataclass
from functools import cached_property, reduce
from typing import NamedTuple, Sequence, Tuple

Params = Tuple[int, int]
RSAModulus = Tuple[int, int, int, int]
Share = Tuple[int, int]
VerificationKey = Tuple[int, int]


class Decryption(NamedTuple):
    share: Share
    proof: int


@dataclass(frozen=True)
class ThresholdPublicKey:
    p: int
    q: int
    v: int

    @cached_property
    def n(self):
        return self.p * self.q

    @cached_property
    def ns1(self):
        return self.n * self.n

    def encrypt(self, message: int) -> int:
        p, q, n, ns1 = self.p, self.q, self.n, self.ns1
        r = pow(p, -1, q)
        return (pow(n + 1, message, ns1) * pow(r, n, ns1)) % ns1

    def encrypt_ballot(self, votes: Sequence[int]) -> Sequence[int]:
        return [self.encrypt(message) for message in votes]

    def partial_sum(self, votes: Sequence[Sequence[int]]) -> Sequence[int]:
        ns1 = self.ns1

        def combine(x, y):
            return (x * y) % ns1

        return [reduce(combine, choice) for choice in zip(*votes)]
