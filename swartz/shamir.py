import functools
import operator
import secrets
from fractions import Fraction
from math import factorial
from typing import Collection

from .types import Params, Share, ThresholdPublicKey


def generate_shares(secret: int, params: Params, nλ: int) -> Collection[Share]:
    """Divides secret using Shamir's secret sharing"""
    k, n = params
    poly = (secret, *(secrets.randbelow(nλ - 1) + 1 for _ in range(k - 1)))
    return tuple((i, eval_poly(poly, i) % nλ) for i in range(1, n + 1))


def combine_shares(shares: Collection[Share], nλ: int) -> int:
    """Combines Shamir's secret sharing shares into original secret

    Wikipedia examples
    >>> nλ = 2 ** 127 - 1  # arbitrarily large prime
    >>> shares = [(1, 1494), (2, 1942), (3, 2578), (4, 3402), (5, 4414), \
        (6, 5614)]
    >>> combine_shares(shares[:3], nλ)
    1234
    >>> combine_shares(shares[-3:], nλ)
    1234

    Every single permutation is possible
    >>> from itertools import permutations
    >>> all(combine_shares(s, nλ) == 1234 for s in permutations(shares, 3))
    True

    Parameters can be arbitrarily huge (thanks to Fraction), and using extra
    shares does impact the result.
    >>> secret = secrets.randbelow(nλ)
    >>> shares = generate_shares(secret, (120, 200), nλ)
    >>> combine_shares(shares, nλ) == secret
    True
    """
    secret = sum(product((Fraction(j, j - i) for j, _ in shares if i != j), x) for i, x in shares)
    return int(secret) % nλ


def combine_threshold_shares(shares: Collection[Share], key: ThresholdPublicKey, params: Params) -> int:
    """
    >>> key = ThresholdPublicKey(5, 7, 0)
    >>> combine_threshold_shares(((1, 1121), (2, 771), (3, 106)), key, (3, 3))
    29
    """
    threshold, servers = params
    assert len(shares) >= threshold

    Δ, n, ns1 = factorial(servers), key.n, key.ns1
    c = product(pow(x, int(2 * Δ * product(Fraction(-j, i - j) for j, _ in shares if i != j)), ns1) for i, x in shares)
    return int(Fraction(c - 1, n) * pow(4 * Δ * Δ, -1, ns1)) % n


def eval_poly(poly: Collection[int], x: int) -> int:
    """Evaluates a polynom with coefficients `poly` on `x`

    >>> poly = [4, 3, 2]
    >>> eval_poly(poly, 0)
    4
    >>> eval_poly(poly, 5)
    69
    """
    return sum(f * x ** i for i, f in enumerate(poly))


product = functools.partial(functools.reduce, operator.mul)
